from abc import ABC, abstractmethod
from uuid import UUID
from common.data_structures.message import Message
from common.data_structures.move import Move


class I_IHMGameCallsComm(ABC):
    @abstractmethod
    def send_message(self, message: Message) -> None:
        pass

    @abstractmethod
    def place_tile(self, move: Move) -> None:
        pass

    @abstractmethod
    def quit_spectator_interface(self, user_id: UUID, game_id: UUID) -> None:
        pass
