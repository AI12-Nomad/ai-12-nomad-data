import uuid as uuid_lib


class Profile:
    """
    Data class for player's publicly visble profiles
    Content can be sent thru the network.

    At init : for a random uuid, don't set a uuid value, or set it at 0/false

    """

    def __init__(
        self,
        nickname: str,
        server_name: str,
        server_port: int,
        games_won: int = 0,
        games_lost: int = 0,
        games_draw: int = 0,
        predefined_uuid: uuid_lib.UUID = uuid_lib.uuid4(),
    ):
        self.nickname = nickname
        self.server_name = server_name
        self.games_won = games_won
        self.games_lost = games_lost
        self.games_draw = games_draw
        self.server_port = server_port

    def __repr__(self):
        return f"Profile: {self.nickname} , uuid : {self.uuid}"

    def __str__(self):
        return f"Profile: {self.nickname}, id : {self.uuid}"


class Player:
    """
    READ ME BEFORE USAGE PLEASE

    This data class is made for user identification, and will accompany a user-associated action sent thru the network,
    such as messages or nomad-moves.

    It's been designed and validated so I coded it.
    But honestly, please follow good practices and common sense, and don't use it.

    If you want to identify a user, just send their UUID.
    Else, if you want people to be able to change their nickname during the game and break our code,
    please use this class ;)

    """

    def __init__(self, nickname: str, uuid: uuid_lib.UUID):
        self.nickname = nickname
        self.uuid = uuid


if __name__ == "__main__":
    p = Profile("Joris", "server", 231)

    print(f"p is {p}")
