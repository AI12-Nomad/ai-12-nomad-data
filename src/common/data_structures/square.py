class Square:
    """
    Data class describing the content of a square of the board.
    """

    def __init__(self, nb_tiles: int, column: int, row: int, tower: bool, color: str):
        self.nb_tiles = nb_tiles
        self.column = column
        self.row = row
        self.tower = tower
        self.color = color

    def __repr__(self):
        col_letter = chr(ord("A") + self.column)
        content = (
            "Tower" if self.tower else f"{self.nb_tiles} tiles, {self.color} on top."
        )
        return (
            f"Square {col_letter}{self.row + 1}"
            f"coord: row{self.row}, col{self.column}"
            f" content: {content}"
        )

    def __str__(self):
        col_letter = chr(ord("A") + self.column)
        content = (
            "Tower" if self.tower else f"{self.nb_tiles} tiles, {self.color} on top."
        )
        return (
            f"Square {col_letter}{self.row + 1}"
            f"coord: row{self.row}, col{self.column}"
            f" content: {content}"
        )
