from common.interfaces.i_ihm_game_calls_ihm_main import I_IHMGameCallsIHMMain


class IHMGameCallsIHMMain_Impl(I_IHMGameCallsIHMMain):

    """

    Interface IHM Game Calls IHM Main

    """

    def ihm_game_stoped(self) -> None:
        pass
