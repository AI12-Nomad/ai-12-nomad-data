from common.interfaces.i_comm_calls_ihm_main import I_CommCallsIHMMain
from common import Profile


class CommCallsIHMMain_Impl(I_CommCallsIHMMain):

    """

    Implementation of Communications Calls IHM Main

    """

    def close_waiting_game(self) -> None:
        pass

    def display_user_profile(self, profile: Profile) -> None:
        pass

    def start_waiting_screen(self) -> None:
        pass

    def launch_spectate_game(self) -> None:
        pass

    def join_game_as_player(self) -> None:
        pass

    def launch_main(self) -> None:
        pass

    def notify_new_connected_user(self) -> None:
        pass

    def player_join_game(self) -> None:
        pass

    def notify_new_game(self) -> None:
        pass

    def notify_game_deleted(self) -> None:
        pass
