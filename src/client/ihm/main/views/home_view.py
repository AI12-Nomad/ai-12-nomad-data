import typing
import pygame

import pygame_gui

from client.ihm.common.ui_renderer import UIRenderer
from client.ihm.common.view import View
from client.ihm.main.components.main_app_title import MainAppTitle
from client.ihm.main.components.home_panel import HomePanel
from client.ihm.main.components.section_title import SectionTitle
from client.ihm.main.components.players_list import PlayersList
from client.ihm.main.components.games_list import GamesList
from client.ihm.main.components.new_game_button import NewGameButton
from client.ihm.main.components.server_infos import ServerInfos
from client.ihm.main.components.profil_button import ProfilButton
from client.ihm.main.components.disconnect_button import DisconnectButton
from client.ihm.main.components.waiting_game_popup import WaitingGamePopup

# from client.ihm.main.ihm_main_controller import IHMMainController


class HomeView(View):
    def __init__(
        self,
        pygame_manager: pygame_gui.UIManager,
        ui: UIRenderer,
        controller: typing.Any,
    ):
        super().__init__(pygame_manager, ui, controller)

        self.main_app_title = MainAppTitle(pygame_manager)
        self.home_panel = HomePanel(pygame_manager)
        self.connected_user_section_title = SectionTitle(
            pygame_manager, self.home_panel, "Utilisateurs connectés", 0, 0
        )
        self.players_list = PlayersList(
            pygame_manager,
            self.home_panel,
            [
                "Michel",
                "Jean",
                "Pierre",
                "Josette",
                "Maurice",
                "Lucien",
                "Géraldine",
                "Monique",
                "Raymond",
                "Jeanne",
                "Louis",
                "Irène",
                "Colette",
                "Ernest",
                "Achille",
            ],
            20,
            75,
        )
        self.live_games_section_title = SectionTitle(
            pygame_manager, self.home_panel, "Parties en cours", 350, 0
        )
        self.games_list = GamesList(
            pygame_manager,
            self.home_panel,
            [
                ("Michel", "Jean"),
                ("Pierre", "En attente ..."),
                ("Josette", "Maurice"),
                ("Lucien", "Géraldine"),
                ("Monique", "Raymond"),
                ("Louis", "En attente ..."),
                ("Irène", "Colette"),
                ("Ernest", "Achille"),
            ],
            370,
            75,
        )
        self.new_game_button = NewGameButton(pygame_manager, self.home_panel)
        self.server_infos = ServerInfos(pygame_manager)
        self.profile_button = ProfilButton(pygame_manager)
        self.disconnect_button = DisconnectButton(pygame_manager)
        self.waiting_game_popup = WaitingGamePopup(pygame_manager)

        self.add(self.main_app_title)
        self.add(self.home_panel)
        self.add(self.connected_user_section_title)
        self.add(self.players_list)
        self.add(self.live_games_section_title)
        self.add(self.games_list)
        self.add(self.new_game_button)
        self.add(self.server_infos)
        self.add(self.profile_button)
        self.add(self.disconnect_button)
        self.add(self.waiting_game_popup)
