import pygame
import pygame_gui

from pygame_gui.core.ui_element import ObjectID

from client.ihm.common.component import Component


class NewGameButton(Component):
    def __init__(
        self, pygame_manager: pygame_gui.UIManager, container: Component
    ) -> None:
        super().__init__(pygame_manager)
        self.pygame_manager = pygame_manager
        self.container = container
        self.text = "Lancer une partie"

    def render(self) -> None:
        self.gui_element = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((490, 440), (250, 75)),
            text=self.text,
            container=self.container.get_gui_element(),
            manager=self.pygame_manager,
            starting_height=1,
            object_id=ObjectID(class_id="@big_red_button"),
        )

    def modify_text(self, text: str) -> None:
        self.text = text
        self.render()
