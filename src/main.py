import time
from threading import Thread

from config import config
from client.ihm.common.py_game_controller import PyGameController
from client.ihm.main.ihm_main_controller import IHMMainController


def main():

    # do anything before the IHM loop (like instantiation of other controllers)
    config.load_config()

    # instantiate ihm controllers
    pygame_controller = PyGameController()
    ihm_main_controller = IHMMainController(pygame_controller)
    # initialize IhmGameController here given the pygame_controller

    # initialize network listeners ON A CHILD THREAD
    # code example
    # network_thread = Thread(target=network_manager.start_listeners)
    # network_thread.start()

    # launch the IHM main loop in the main thread
    # /!\ windows & macos, doesn't allow to execute the pygame's main loop on a child process
    # so network callbacks should be instanced on a separate thread before this call
    pygame_controller.run_main_loop()  # blocking call, will stop on user closing the pygame's window

    # saving things, close network thread and close connections


if __name__ == "__main__":
    main()
